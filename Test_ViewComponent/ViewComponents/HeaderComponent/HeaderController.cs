﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Test_ViewComponent.ViewComponents.HeaderComponent
{
    [ViewComponent(Name = "Header")]
    public partial class HeaderController : ViewComponent
    {
        //调用异步方法
        public async Task<IViewComponentResult> InvokeAsync(int id) {
            //返回参数，Inex是自己定义的视图名称，如果没有就是default
            //第二个是返回参数，和之前的view()的参数一样。
            return  View("Index",id);
        }
    }
}