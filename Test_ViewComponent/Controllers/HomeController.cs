﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Test_ViewComponent.Controllers
{
    public  class HomeController : Controller
    {
        /// <summary>
        /// 显示首页信息
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 控制器调用
        /// </summary>
        /// <returns></returns>
        public IActionResult Info() {
            return ViewComponent("Header", 5);
        }
    }
}